window.data = {
  google: 0
}

browser.webRequest.onBeforeRequest.addListener(
  checkUrl,
  { urls: [ '<all_urls>' ] }
)

// Thanks: https://stackoverflow.com/a/25703406
const hostnameRegex = /^(?:https?:\/\/)?(?:[^@\/\n]+@)?(?:www\.)?([^:\/?\n]+)/

function checkUrl (requestDetails) {
  const url = requestDetails.url


  const matches = hostnameRegex.exec(url)
  if (!matches || !matches[1]) return

  const hostname = matches[1]

  console.log('Checking %s -> %s', url, hostname)

  if (isGoogle(hostname)) {
    window.data.google += 1

    notifyUpdate()
  }
}

function notifyUpdate () {
  browser.runtime.sendMessage({
    type: 'update'
  })
}

function isGoogle (hostname) {
  const allDomains = [
    'google',
    'youtube',
  ]

  for (const domain of allDomains) {
    if (hostname.startsWith(domain + '.') || hostname.includes('.' + domain + '.') ) {
      return true
    }
  }

  const endsDomain = [
    'gstatic.com',
    'android.com',
    'youtu.be',
    'doubleclick.com',
    'adsense.com',
    'googleanalytics.com',
    'google-analytics.com',
    'googledrive.com',
    'googlemaps.com',
    'gmail.com',
    'goo.gl',
    'googleusercontent.com',
    'googlevideo.com'
  ]

  for (const domain of endsDomain) {
    if (hostname.endsWith(domain)) {
      return true
    }
  }

  return false
}
