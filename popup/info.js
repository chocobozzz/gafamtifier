const data = browser.extension.getBackgroundPage().window.data

const labels = {
  'google': 'Google'
}

const contentDiv = document.getElementById('content')
rebuildPage()

browser.runtime.onMessage.addListener(handleMessage);

function handleMessage(message) {
  if (message.type === 'update') rebuildPage()
}

function rebuildPage () {
  contentDiv.innerHTML = ''

  for (const key of Object.keys(data)) {
    const value = data[key]
    const label = labels[key]

    const div = document.createElement('div')
    div.innerText = label + ': ' + value

    contentDiv.appendChild(div)
  }
}
